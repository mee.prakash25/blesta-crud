<?php
$lang['AdminMain.index.page_title'] = 'Live Stream Helper - AdminMain';

$lang['AdminMain.index.boxtitle'] = 'Helper Objects';

$lang['AdminMain.!success.object_added'] = 'Object added successfully!';

$lang['AdminMain.index.heading_add_object'] = 'Add Object';
$lang['AdminMain.index.heading_objects'] = 'Objects';
$lang['AdminMain.index.heading_name'] = 'Object Name';

$lang['AdminMain.index.field_name'] = 'Object Name';

$lang['AdminMain.index.no_results'] = 'There are currently no objects in the system';

$lang['AdminMain.index.submit'] = 'Submit';
