<?php
/**
 * en_us language for the Live Stream Helper plugin.
 */
// Basics
$lang['LiveStreamHelperPlugin.name'] = 'Live Stream Helper';
$lang['LiveStreamHelperPlugin.description'] = '';


// Plugin Actions
$lang['LiveStreamHelperPlugin.nav_primary_staff.index'] = 'Helper Objects';
$lang['LiveStreamHelperPlugin.nav_primary_client.index'] = 'Helper Objects';