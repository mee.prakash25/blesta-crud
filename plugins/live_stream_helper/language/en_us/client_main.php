<?php
$lang['ClientMain.index.page_title'] = 'Live Stream Helper - Client Main';

$lang['ClientMain.index.boxtitle'] = 'Helper Objects';

$lang['ClientMain.!success.object_added'] = 'Object added successfully!';

$lang['ClientMain.index.heading_add_object'] = 'Add Object';
$lang['ClientMain.index.heading_objects'] = 'Objects';
$lang['ClientMain.index.heading_name'] = 'Object Name';

$lang['ClientMain.index.field_name'] = 'Object Name';

$lang['ClientMain.index.no_results'] = 'There are currently no objects in the system';

$lang['ClientMain.index.submit'] = 'Submit';
