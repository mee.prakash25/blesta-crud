
<?php
/**
 * View Tools Test client_view controller
 *
 * @link https://www.blesta.com Blesta
 */
class ClientMain extends LiveStreamHelperController
{
    /**
     * Setup
     */
    public function preAction()
    {
        parent::preAction();

        $this->structure->set('page_title', Language::_('ClientMain.index.page_title', true));
    }

    /**
     * Returns the view for a list of objects
     */
    public function index()
    {
        $this->uses(['LiveStreamHelper.LiveStreamHelperObjects']);
        $page = (isset($this->get[1]) ? (int) $this->get[1] : 1);
        $sort = (isset($this->get['sort']) ? $this->get['sort'] : 'name');
        $order = (isset($this->get['order']) ? $this->get['order'] : 'desc');

        if (!empty($this->post)) {
            $this->LiveStreamHelperObjects->add($this->post);
            if (($errors = $this->LiveStreamHelperObjects->errors())) {
                $this->setMessage('error', $errors, false, null, false);
            } else {
                $this->flashMessage('message', Language::_('ClientMain.!success.object_added', true), null, false);
                $this->redirect($this->base_uri . 'plugin/live_stream_helper/client_main/index/' . $page);
            }
        }

        $this->set('objects', $this->LiveStreamHelperObjects->getList([], $page, [$sort => $order]));
        $this->set('sort', $sort);
        $this->set('order', $order);
        $this->set('negate_order', ($order == 'asc' ? 'desc' : 'asc'));
        $total_results = $this->LiveStreamHelperObjects->getListCount([]);

        // Overwrite default pagination settings
        $settings = array_merge(
            Configure::get('Blesta.pagination'),
            [
                'total_results' => $total_results,
                'uri' => $this->base_uri . 'plugin/live_stream_helper/client_main/index/[p]/',
                'params' => ['sort' => $sort, 'order' => $order],
            ]
        );
        $this->setPagination($this->get, $settings);

        // Render the request if ajax
        return $this->renderAjaxWidgetIfAsync(isset($this->get[1]) || isset($this->get['sort']));
    }
}
