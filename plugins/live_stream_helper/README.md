# Live Stream Helper



## Install the Plugin

1. You can install the plugin via composer:

    ```
    composer require parent_repository/live_stream_helper
    ```

2. OR upload the source code to a /plugins/live_stream_helper/ directory within
your Blesta installation path.

    For example:

    ```
    /var/www/html/blesta/plugins/live_stream_helper/
    ```

3. Log in to your admin Blesta account and navigate to
> Settings > Plugins

4. Find the Live Stream Helper plugin and click the "Install" button to install it

5. You're done!

