<?php
/**
 * live_stream_helper_objects Management
 *
 * @link http://www.blesta.com/  Blesta
 */
class LiveStreamHelperObjects extends LiveStreamHelperModel
{
    /**
     * Returns a list of records for the given company
     *
     * @param array $filters A list of filters for the query
     *
     *  - id
     *  - name
     * @param int $page The page number of results to fetch
     * @param array $order A key/value pair array of fields to order the results by
     * @return array An array of stdClass objects
     */
    public function getList(
        array $filters = [],
        $page = 1,
        array $order = ['id' => 'desc']
    ) {
        $records = $this->getRecord($filters)
            ->order($order)
            ->limit($this->getPerPage(), (max(1, $page) - 1) * $this->getPerPage())
            ->fetchAll();

        return $records;
    }

    /**
     * Returns the total number of record for the given filters
     *
     * @param array $filters A list of filters for the query
     *
     *  - id
     *  - name
     * @return int The total number of records for the given filters
     */
    public function getListCount(array $filters = [])
    {
        return $this->getRecord($filters)->numResults();
    }

    /**
     * Returns all records in the system for the given filters
     *
     * @param array $filters A list of filters for the query
     *
     *  - id
     *  - name
     * @param array $order A key/value pair array of fields to order the results by
     * @return array An array of stdClass objects
     */
    public function getAll(
        array $filters = [],
        array $order = ['id' => 'desc']
    ) {
        $records = $this->getRecord($filters)->order($order)->fetchAll();

        return $records;
    }

    /**
     * Fetches the record with the given identifier
     *
     * @param int $id The identifier of the record to fetch
     * @return mixed A stdClass object representing the record, false if no such record exists
     */
    public function get($id)
    {
        $record = $this->getRecord(['id' => $id])->fetch();

        return $record;
    }

    /**
     * Add a record
     *
     * @param array $vars An array of input data including:
     *
     *  - id
     *  - name
     * @return int The identifier of the record that was created, void on error
     */
    public function add(array $vars)
    {
        $this->Input->setRules($this->getRules($vars));

        if ($this->Input->validates($vars)) {
            $fields = ['id','name'];
            $this->Record->insert('live_stream_helper_objects', $vars, $fields);

            return $this->Record->lastInsertId();
        }
    }

    /**
     * Edit a record
     *
     * @param int $id The identifier of the record to edit
     * @param array $vars An array of input data including:
     *
     *  - id
     *  - name
     * @return int The identifier of the record that was updated, void on error
     */
    public function edit($id, array $vars)
    {

        $vars['id'] = $id;
        $this->Input->setRules($this->getRules($vars, true));

        if ($this->Input->validates($vars)) {
            $fields = ['id','name'];
            $this->Record->where('id', '=', $id)->update('live_stream_helper_objects', $vars, $fields);

            return $id;
        }
    }

    /**
     * Permanently deletes the given record
     *
     * @param int $id The identifier of the record to delete
     */
    public function delete($id)
    {
        // Delete a record
        $this->Record->from('live_stream_helper_objects')->
            where('live_stream_helper_objects.id', '=', $id)->
            delete();
    }

    /**
     * Returns a partial query
     *
     * @param array $filters A list of filters for the query
     *
     *  - id
     *  - name
     * @return Record A partially built query
     */
    private function getRecord(array $filters = [])
    {
        $this->Record->select()->from('live_stream_helper_objects');

        if (isset($filters['id'])) {
            $this->Record->where('live_stream_helper_objects.id', '=', $filters['id']);
        }

        if (isset($filters['name'])) {
            $this->Record->where('live_stream_helper_objects.name', '=', $filters['name']);
        }

        return $this->Record;
    }

    /**
     * Returns all validation rules for adding/editing extensions
     *
     * @param array $vars An array of input key/value pairs
     *
     *  - id
     *  - name
     * @param bool $edit True if this if an edit, false otherwise
     * @return array An array of validation rules
     */
    private function getRules(array $vars, $edit = false)
    {
        $rules = [
            'id' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => true,
                    'message' => Language::_('LiveStreamHelperObjects.!error.id.valid', true)
                ]
            ],
            'name' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => 'isEmpty',
                    'negate' => true,
                    'message' => Language::_('LiveStreamHelperObjects.!error.name.valid', true)
                ]
            ]
        ];

        return $rules;
    }
}
