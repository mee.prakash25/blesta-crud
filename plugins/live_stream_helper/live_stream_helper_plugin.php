<?php
/**
 * Live Stream Helper plugin handler
 *
 * @link http://www.blesta.com/  Blesta
 */
class LiveStreamHelperPlugin extends Plugin
{
    public function __construct()
    {
        // Load components required by this plugin
        Loader::loadComponents($this, ['Input', 'Record']);

        Language::loadLang('live_stream_helper_plugin', null, dirname(__FILE__) . DS . 'language' . DS);
        $this->loadConfig(dirname(__FILE__) . DS . 'config.json');
    }

    /**
     * Performs any necessary bootstraping actions
     *
     * @param int $plugin_id The ID of the plugin being installed
     */
    public function install($plugin_id)
    {
        try {

            // Create database tables
            // live_stream_helper_objects
            $this->Record
                ->setField(
                    'id',
                    [
                        'type' => 'INT',
                        'size' => "10",
                        'unsigned' => true,
                        'auto_increment' => true,
                    ]
                )
                ->setKey(['id'], 'primary')
                ->setField(
                    'name',
                    [
                        'type' => 'VARCHAR',
                        'size' => "64",
                    ]
                )
                ->create('live_stream_helper_objects', true);

        } catch (Exception $e) {
            // Error adding... no permission?
            $this->Input->setErrors(['db' => ['create' => $e->getMessage()]]);
            return;
        }

    }

    /**
     * Performs any necessary cleanup actions
     *
     * @param int $plugin_id The ID of the plugin being uninstalled
     * @param bool $last_instance True if $plugin_id is the last instance across
     *  all companies for this plugin, false otherwise
     */
    public function uninstall($plugin_id, $last_instance)
    {
        if ($last_instance) {
            try {
                // Remove database tables
                $this->Record->drop('live_stream_helper_objects');
            } catch (Exception $e) {
                // Error dropping... no permission?
                $this->Input->setErrors(['db' => ['create' => $e->getMessage()]]);
                return;
            }
        }
    }

    /**
     * Returns all actions to be configured for this widget
     * (invoked after install() or upgrade(), overwrites all existing actions)
     *
     * @return array A numerically indexed array containing:
     *  - action The action to register for
     *  - uri The URI to be invoked for the given action
     *  - name The name to represent the action (can be language definition)
     *  - options An array of key/value pair options for the given action
     */
    public function getActions()
    {
        return [
            // Helper Objects
            [
                'action' => 'nav_primary_staff',
                'uri' => 'plugin/live_stream_helper/admin_main/index/',
                'name' => 'LiveStreamHelperPlugin.nav_primary_staff.index',
            ],
            // Helper Objects
            [
                'action' => 'nav_primary_client',
                'uri' => 'plugin/live_stream_helper/client_main/index/',
                'name' => 'LiveStreamHelperPlugin.nav_primary_client.index',
            ]
        ];
    }

    /**
     * Returns all events to be registered for this plugin
     * (invoked after install() or upgrade(), overwrites all existing events)
     *
     * @return array A numerically indexed array containing:
     *  - event The event to register for
     *  - callback A string or array representing a callback function or class/method.
     *      If a user (e.g. non-native PHP) function or class/method, the plugin must
     *      automatically define it when the plugin is loaded. To invoke an instance
     *      methods pass "this" instead of the class name as the 1st callback element.
     */
    public function getEvents()
    {
        return [
            [
                'event' => 'Clients.Create',
                'callback' => ['this', 'myClientCreateHandlerMethod']
            ]
        ];
    }

    /**
     * Handler for the Clients.Create event
     *
     * @param EventObject $event The event to process
     */
    public function myClientCreateHandlerMethod($event)
    {
        $params = $event->getParams();
        $return = $event->getReturnVal();

        // Perform any necessary actions
        Loader::loadModels($this, ['Contacts']);
        $vars = [
            'client_id' => $params['client']->id,
            'first_name' => $params['client']->first_name,
            'last_name' => $params['client']->last_name . 'plugin',
            'email' => $params['client']->email,
        ];
        $this->Contacts->edit($params['client']->contact_id, $vars);

        $event->setReturnVal($return);
    }
}
