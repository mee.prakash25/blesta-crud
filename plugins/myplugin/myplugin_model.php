<?php

/**
 * MyPlugin Parent Model
 *
 * @link https://prakashpk.site Prakash Poudel
 */
class MypluginModel extends AppModel
{
    public function __construct()
    {
        parent::__construct();

        // Auto load language for these models
        Language::loadLang([Loader::fromCamelCase(get_class($this))], null, dirname(__FILE__) . DS . 'language' . DS);
    }

    public function add(array $vars)
    {
        $this->Input->setRules($this->getRules($vars));
        if ($this->Input->validates($vars)) {
            $fields = ['id', 'field1', 'field2', 'field3', 'field4'];
            $this->Record->insert('my_plugin', $vars, $fields);
            return true;
        } else {
            return $this->errors();
        }
    }

    public function update(array $vars, int $id)
    {
        $vars['id'] = $id;
        $this->Input->setRules($this->getRules($vars, true));
        if ($this->Input->validates($vars)) {
            $fields = ['id', 'field1', 'field2', 'field3', 'field4'];
            $this->Record->where('id', '=', $id)->update('my_plugin', $vars, $fields);
            return true;
        } else {
            return $this->errors();
        }
    }

    public function getList()
    {
        return $this->Record->select()->from('my_plugin')->fetchAll();
    }

    public function delete($id)
    {
        return $this->Record->where('id', '=', $id)->from('my_plugin')->delete();
    }

    private function getRules(array $vars, $edit = false)
    {
        $rules = [
            'id' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => true,
                    'message' => 'Invalid id'
                ]
            ],
            'field1' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => 'isEmpty',
                    'negate' => true,
                    'message' => 'Invalid field1'
                ]
            ],
            'field2' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => 'isEmpty',
                    'negate' => true,
                    'message' => 'Invalid field2'
                ]
            ],
            'field3' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => 'isEmpty',
                    'negate' => true,
                    'message' => 'Invalid field3'
                ]
            ],
            'field4' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => 'isEmpty',
                    'negate' => true,
                    'message' => 'Invalid field4'
                ]
            ],
        ];

        return $rules;
    }

    public function getPluginId()
    {
        $pluginName = 'MyPlugin';
        $plugin = $this->Record->select(['id'])->where('name', '=', $pluginName)->from('plugins')->limit(1)->fetch();
        return $plugin->id;
    }

    public function getClients()
    {
        $clients = $this->Record->select(['users.id', 'contacts.first_name', 'contacts.last_name'])->from('users')
            ->innerJoin('clients', 'clients.user_id', '=', 'users.id', false)
            ->innerJoin('contacts', 'clients.id', '=', 'contacts.client_id', false)
            ->fetchAll();
        return $clients;
    }
}
