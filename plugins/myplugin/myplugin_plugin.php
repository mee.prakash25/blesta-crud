<?php

/**
 * MyPlugin plugin handler
 *
 * @link https://prakashpk.site Prakash Poudel
 */
class MypluginPlugin extends Plugin
{
    public function __construct()
    {
        // Load components required by this plugin
        Loader::loadComponents($this, ['Input', 'Record']);

        Language::loadLang('myplugin_plugin', null, dirname(__FILE__) . DS . 'language' . DS);
        $this->loadConfig(dirname(__FILE__) . DS . 'config.json');
    }

    /**
     * Performs any necessary bootstraping actions
     *
     * @param int $plugin_id The ID of the plugin being installed
     */
    public function install($plugin_id)
    {
        try {
            $this->Record
                ->setField(
                    'id',
                    [
                        'type' => 'INT',
                        'size' => '10',
                        'unsigned' => true,
                        'auto_increment' => true
                    ]
                )
                ->setKey(['id'], 'primary')
                ->setField(
                    'field1',
                    [
                        'type' => 'VARCHAR',
                        'size' => '256'
                    ]
                )
                ->setField(
                    'field2',
                    [
                        'type' => 'VARCHAR',
                        'size' => '256'
                    ]
                )
                ->setField(
                    'field3',
                    [
                        'type' => 'VARCHAR',
                        'size' => '256'
                    ]
                )
                ->setField(
                    'field4',
                    [
                        'type' => 'VARCHAR',
                        'size' => '256'
                    ]
                )
                ->create('my_plugin', true);

            $this->Record->insert('my_plugin', ['field1' => '1', 'field2' => '2', 'field3' => '3', 'field4' => '4'], ['id', 'field1', 'field2', 'field3', 'field4']);
        } catch (Exception $e) {
            $this->Input->setErrors(['db' => ['create' => $e->getMessage()]]);
            return;
        }
    }

    /**
     * Performs any necessary cleanup actions
     *
     * @param int $plugin_id The ID of the plugin being uninstalled
     * @param bool $last_instance True if $plugin_id is the last instance across
     *  all companies for this plugin, false otherwise
     */
    public function uninstall($plugin_id, $last_instance)
    {
        if ($last_instance) {
            try {
                // Remove database tables
                $this->Record->drop('my_plugin');
            } catch (Exception $e) {
                // Error dropping... no permission?
                $this->Input->setErrors(['db' => ['create' => $e->getMessage()]]);
                return;
            }
        }
    }

    public function getActions()
    {
        return [
            // Helper Objects
            [
                'action' => 'nav_primary_staff',
                'uri' => '#',
                'name' => 'My Plugin',
                'options' => [
                    'sub' => [
                        [
                            'uri' => 'plugin/myplugin/admin_main/index/',
                            'name' => 'Fields CRUD'
                        ],
                        [
                            'uri' => 'plugin/myplugin/admin_main/clients/',
                            'name' => 'Client List'
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * Returns all permissions to be configured for this plugin (invoked after install(), upgrade(),
     *  and uninstall(), overwrites all existing permissions)
     *
     * @return array A numerically indexed array containing:
     *
     *  - group_alias The alias of the permission group this permission belongs to
     *  - name The name of this permission
     *  - alias The ACO alias for this permission (i.e. the Class name to apply to)
     *  - action The action this ACO may control (i.e. the Method name of the alias to control access for)
     */
    public function getPermissions()
    {
    }

    /**
     * Returns all permission groups to be configured for this plugin (invoked after install(), upgrade(),
     *  and uninstall(), overwrites all existing permission groups)
     *
     * @return array A numerically indexed array containing:
     *
     *  - name The name of this permission group
     *  - level The level this permission group resides on (staff or client)
     *  - alias The ACO alias for this permission group (i.e. the Class name to apply to)
     */
    public function getPermissionGroups()
    {
    }
}
