# MyPlugin

My Test Task plugin

## Install the Plugin

1. You can install the plugin via composer:

    ```
    composer require parent_repository/myplugin
    ```

2. OR upload the source code to a /plugins/myplugin/ directory within
your Blesta installation path.

    For example:

    ```
    /var/www/html/blesta/plugins/myplugin/
    ```

3. Log in to your admin Blesta account and navigate to
> Settings > Plugins

4. Find the MyPlugin plugin and click the "Install" button to install it

5. You're done!

