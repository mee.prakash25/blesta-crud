<?php
class AdminManagePlugin extends AppController {

    /**
     * Performs necessary initialization
     */
    private function init()
    {
        // Set the view to render for all actions under this controller
        $this->view->setView(null, 'myplugin.default');
    }

    public function index() {
        $this->init();
//        $pattern = '/\/(\d+)\/$/';
//        $url = $_SERVER['REQUEST_URI'];
//        preg_match($pattern, $url, $matches);
//
//        if (isset($matches[1])) {
//            $pluginId = $matches[1];
//        }

        return $this->partial("admin_manage_plugin");
    }
}
?>