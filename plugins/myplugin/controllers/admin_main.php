<?php

class AdminMain extends MypluginController
{
    private function init()
    {
        // Set the view to render for all actions under this controller
        $this->view->setView(null, 'myplugin.default');
    }

    public function preAction()
    {
        parent::preAction();

        $this->structure->set('page_title', Language::_('AdminMain.index.page_title', true));
    }

    public function index()
    {
        $this->uses(['MypluginModel']);
        $pluginId = $this->MypluginModel->getPluginId();
        if (!empty($this->post)) {
            $message = '';
            $id = $this->post['id'];
            $fields = ['field1' => $this->post['field1'], 'field2' => $this->post['field2'], 'field3' => $this->post['field3'], 'field4' => $this->post['field4']];
            if (!empty($id)) {
                $result = $this->MypluginModel->update($fields, $id);
                if ($result === true) {
                    $message = 'Field updated successfully';
                    $this->flashMessage('message', $message, null, false);
                } else {
                    foreach ($result as $field => $error) {
                        $message .= $error['valid'] . ', ';
                    }
                    $this->flashMessage('error', $message, null, false);
                }
            } else {
                $result = $this->MypluginModel->add($fields);
                if ($result === true) {
                    $message = 'Field added successfully';
                    $this->flashMessage('message', $message, null, false);
                } else {
                    foreach ($result as $field => $error) {
                        $message .= $error['valid'] . ', ';
                    }
                    $this->flashMessage('error', $message, null, false);
                    $this->redirect($this->base_uri . 'settings/company/plugins/manage/' . $pluginId);
                }
            }
            $this->redirect($this->base_uri . 'plugin/myplugin/admin_main/index/');
        }
        $this->set('addUrl', $this->base_uri . 'settings/company/plugins/manage/' . $pluginId);
        $this->set('fields', $this->MypluginModel->getList());
    }

    public function delete()
    {
        $this->uses(['MypluginModel']);
        $this->MypluginModel->delete($this->get['id']);

        $this->flashMessage('message', 'Field deleted successfully', null, false);
        $this->redirect($this->base_uri . 'plugin/myplugin/admin_main/index/');
    }

    public function clients()
    {
        $this->init();
        $this->uses(['MypluginModel']);
        $this->set('clients', $this->MypluginModel->getClients());
        return $this->view->fetch('client_list');
    }
}