<?php
/**
 * TestTaskPlugin parent controller
 *
 * @link https://prakashpk.site Prakash Poudel
 */
class TesttaskpluginController extends AppController
{
    /**
     * Require admin to be login and setup the view
     */
    public function preAction()
    {
        $this->structure->setDefaultView(APPDIR);
        parent::preAction();

        // Override default view directory
        $this->view->view = "default";

        $this->requireLogin();

        // Auto load language for the controller
        Language::loadLang(
            [Loader::fromCamelCase(get_class($this))],
            null,
            dirname(__FILE__) . DS . 'language' . DS
        );
        Language::loadLang(
            'testtaskplugin_controller',
            null,
            dirname(__FILE__) . DS . 'language' . DS
        );
    }
}
