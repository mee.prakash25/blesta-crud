# TestTaskPlugin

My Test Task Plugin

## Install the Plugin

1. You can install the plugin via composer:

    ```
    composer require parent_repository/testtaskplugin
    ```

2. OR upload the source code to a /plugins/testtaskplugin/ directory within
your Blesta installation path.

    For example:

    ```
    /var/www/html/blesta/plugins/testtaskplugin/
    ```

3. Log in to your admin Blesta account and navigate to
> Settings > Plugins

4. Find the TestTaskPlugin plugin and click the "Install" button to install it

5. You're done!

