# MyPlugin

My Plugin

## Install the Module

1. You can install the module via composer:

    ```
    composer require parent_repository/myplugin
    ```

2. OR upload the source code to a /components/modules/myplugin/ directory within
your Blesta installation path.

    For example:

    ```
    /var/www/html/blesta/components/modules/myplugin/
    ```

3. Log in to your admin Blesta account and navigate to
> Settings > Modules

4. Find the MyPlugin module and click the "Install" button to install it

5. You're done!

