<?php
/**
 * en_us language for the MyPlugin module.
 */
// Basics
$lang['Myplugin.name'] = 'MyPlugin';
$lang['Myplugin.description'] = 'My Plugin';
$lang['Myplugin.module_row'] = 'MyPlugin';
$lang['Myplugin.module_row_plural'] = 'MyPlugins';
$lang['Myplugin.module_group'] = 'My Plugin Group';


// Module management
$lang['Myplugin.add_module_row'] = 'Add MyPlugin';
$lang['Myplugin.add_module_group'] = 'Add My Plugin Group';
$lang['Myplugin.manage.module_rows_title'] = 'MyPlugins';

$lang['Myplugin.manage.module_rows_heading.options'] = 'Options';
$lang['Myplugin.manage.module_rows.edit'] = 'Edit';
$lang['Myplugin.manage.module_rows.delete'] = 'Delete';
$lang['Myplugin.manage.module_rows.confirm_delete'] = 'Are you sure you want to delete this MyPlugin';

$lang['Myplugin.manage.module_rows_no_results'] = 'There are no MyPlugins.';

$lang['Myplugin.manage.module_groups_title'] = 'Groups';
$lang['Myplugin.manage.module_groups_heading.name'] = 'Name';
$lang['Myplugin.manage.module_groups_heading.module_rows'] = 'MyPlugins';
$lang['Myplugin.manage.module_groups_heading.options'] = 'Options';

$lang['Myplugin.manage.module_groups.edit'] = 'Edit';
$lang['Myplugin.manage.module_groups.delete'] = 'Delete';
$lang['Myplugin.manage.module_groups.confirm_delete'] = 'Are you sure you want to delete this MyPlugin';

$lang['Myplugin.manage.module_groups.no_results'] = 'There is no My Plugin Group';


$lang['Myplugin.order_options.roundrobin'] = 'Evenly Distribute Among Servers';
$lang['Myplugin.order_options.first'] = 'First Non-full Server';


// Add row
$lang['Myplugin.add_row.box_title'] = 'MyPlugin - Add MyPlugin';
////$lang['Myplugin.add_row.name_servers_title'] = 'Name Servers';
////$lang['Myplugin.add_row.notes_title'] = 'Notes';
////$lang['Myplugin.add_row.name_server_btn'] = 'Add Additional Name Server';
////$lang['Myplugin.add_row.name_server_col'] = 'Name Server';
////$lang['Myplugin.add_row.name_server_host_col'] = 'Hostname';
////$lang['Myplugin.add_row.name_server'] = 'Name server %1$s'; // %1$s is the name server number (e.g. 3)
////$lang['Myplugin.add_row.remove_name_server'] = 'Remove';
$lang['Myplugin.add_row.add_btn'] = 'Add MyPlugin';


// Edit row
$lang['Myplugin.edit_row.box_title'] = 'MyPlugin - Edit MyPlugin';
////$lang['Myplugin.edit_row.name_servers_title'] = 'Name Servers';
////$lang['Myplugin.edit_row.notes_title'] = 'Notes';
////$lang['Myplugin.edit_row.name_server_btn'] = 'Add Additional Name Server';
////$lang['Myplugin.edit_row.name_server_col'] = 'Name Server';
////$lang['Myplugin.edit_row.name_server_host_col'] = 'Hostname';
////$lang['Myplugin.edit_row.name_server'] = 'Name server %1$s'; // %1$s is the name server number (e.g. 3)
////$lang['Myplugin.edit_row.remove_name_server'] = 'Remove';
$lang['Myplugin.edit_row.edit_btn'] = 'Update MyPlugin';


// Row meta




// Errors
$lang['Myplugin.!error.module_row.missing'] = 'An internal error occurred. The module row is unavailable.';


// Service info
$lang['Myplugin.service_info.domain'] = 'Domain';
////// These are the definitions for if you are trying to include a login link in the service info pages
////$lang['Myplugin.service_info.options'] = 'Options';
////$lang['Myplugin.service_info.option_login'] = 'Login';

// Service Fields
$lang['Myplugin.service_fields.domain'] = 'Domain';




// Package Fields
$lang['Myplugin.package_fields.epp_code'] = 'EPP Code';


$lang['Myplugin.package_field.tooltip.epp_code'] = 'Whether to allow users to request an EPP Code through the Blesta service interface.';

// Cron Tasks

