<?php
////use Blesta\Core\Util\Validate\Server;
/**
 * MyPlugin Module
 *
 * @link https://prakashpk.site Prakash Poudel
 */
class Myplugin extends Module
{

    /**
     * Initializes the module
     */
    public function __construct()
    {
        // Load the language required by this module
        Language::loadLang('myplugin', null, dirname(__FILE__) . DS . 'language' . DS);

        // Load components required by this module
        Loader::loadComponents($this, ['Input']);

        // Load module config
        $this->loadConfig(dirname(__FILE__) . DS . 'config.json');

        Configure::load('myplugin', dirname(__FILE__) . DS . 'config' . DS);
    }

    /**
     * Performs any necessary bootstraping actions
     */
    public function install()
    {
    }

    /**
     * Performs migration of data from $current_version (the current installed version)
     * to the given file set version. Sets Input errors on failure, preventing
     * the module from being upgraded.
     *
     * @param string $current_version The current installed version of this module
     */
    public function upgrade($current_version)
    {
////        if (version_compare($current_version, '1.1.0', '<')) {
////            // Preform actions here such as re-adding cron tasks, setting new meta fields, and more
////        }
    }

    /**
     * Performs any necessary cleanup actions. Sets Input errors on failure
     * after the module has been uninstalled.
     *
     * @param int $module_id The ID of the module being uninstalled
     * @param bool $last_instance True if $module_id is the last instance
     *  across all companies for this module, false otherwise
     */
    public function uninstall($module_id, $last_instance)
    {
    }

    /**
     * Returns the rendered view of the manage module page.
     *
     * @param mixed $module A stdClass object representing the module and its rows
     * @param array $vars An array of post data submitted to or on the manager module
     *  page (used to repopulate fields after an error)
     * @return string HTML content containing information to display when viewing the manager module page
     */
    public function manageModule($module, array &$vars)
    {
        // Load the view into this object, so helpers can be automatically added to the view
        $this->view = new View('manage', 'default');
        $this->view->base_uri = $this->base_uri;
        $this->view->setDefaultView('components' . DS . 'modules' . DS . 'myplugin' . DS);

        // Load the helpers required for this view
        Loader::loadHelpers($this, ['Form', 'Html', 'Widget']);

        $this->view->set('module', $module);

        return $this->view->fetch();
    }

    /**
     * Returns the rendered view of the add module row page.
     *
     * @param array $vars An array of post data submitted to or on the add module
     *  row page (used to repopulate fields after an error)
     * @return string HTML content containing information to display when viewing the add module row page
     */
    public function manageAddRow(array &$vars)
    {
        // Load the view into this object, so helpers can be automatically added to the view
        $this->view = new View('add_row', 'default');
        $this->view->base_uri = $this->base_uri;
        $this->view->setDefaultView('components' . DS . 'modules' . DS . 'myplugin' . DS);

        // Load the helpers required for this view
        Loader::loadHelpers($this, ['Form', 'Html', 'Widget']);

        if (!empty($vars)) {
            // Set unset checkboxes
            $checkbox_fields = [];

            foreach ($checkbox_fields as $checkbox_field) {
                if (!isset($vars[$checkbox_field])) {
                    $vars[$checkbox_field] = 'false';
                }
            }
        }

        $this->view->set('vars', (object) $vars);

        return $this->view->fetch();
    }

    /**
     * Returns the rendered view of the edit module row page.
     *
     * @param stdClass $module_row The stdClass representation of the existing module row
     * @param array $vars An array of post data submitted to or on the edit
     *  module row page (used to repopulate fields after an error)
     * @return string HTML content containing information to display when viewing the edit module row page
     */
    public function manageEditRow($module_row, array &$vars)
    {
        // Load the view into this object, so helpers can be automatically added to the view
        $this->view = new View('edit_row', 'default');
        $this->view->base_uri = $this->base_uri;
        $this->view->setDefaultView('components' . DS . 'modules' . DS . 'myplugin' . DS);

        // Load the helpers required for this view
        Loader::loadHelpers($this, ['Form', 'Html', 'Widget']);

        if (empty($vars)) {
            $vars = $module_row->meta;
        } else {
            // Set unset checkboxes
            $checkbox_fields = [];

            foreach ($checkbox_fields as $checkbox_field) {
                if (!isset($vars[$checkbox_field])) {
                    $vars[$checkbox_field] = 'false';
                }
            }
        }

        $this->view->set('vars', (object) $vars);

        return $this->view->fetch();
    }

    /**
     * Adds the module row on the remote server. Sets Input errors on failure,
     * preventing the row from being added. Returns a set of data, which may be
     * a subset of $vars, that is stored for this module row.
     *
     * @param array $vars An array of module info to add
     * @return array A numerically indexed array of meta fields for the module row containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     */
    public function addModuleRow(array &$vars)
    {
        $meta_fields = [];
        $encrypted_fields = [];

        // Set unset checkboxes
        $checkbox_fields = [];

        foreach ($checkbox_fields as $checkbox_field) {
            if (!isset($vars[$checkbox_field])) {
                $vars[$checkbox_field] = 'false';
            }
        }

        $this->Input->setRules($this->getRowRules($vars));

        // Validate module row
        if ($this->Input->validates($vars)) {
            // Build the meta data for this row
            $meta = [];
            foreach ($vars as $key => $value) {
                if (in_array($key, $meta_fields)) {
                    $meta[] = [
                        'key' => $key,
                        'value' => $value,
                        'encrypted' => in_array($key, $encrypted_fields) ? 1 : 0
                    ];
                }
            }

            return $meta;
        }
    }

    /**
     * Edits the module row on the remote server. Sets Input errors on failure,
     * preventing the row from being updated. Returns a set of data, which may be
     * a subset of $vars, that is stored for this module row.
     *
     * @param stdClass $module_row The stdClass representation of the existing module row
     * @param array $vars An array of module info to update
     * @return array A numerically indexed array of meta fields for the module row containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     */
    public function editModuleRow($module_row, array &$vars)
    {
        $meta_fields = [];
        $encrypted_fields = [];

        // Set unset checkboxes
        $checkbox_fields = [];

        foreach ($checkbox_fields as $checkbox_field) {
            if (!isset($vars[$checkbox_field])) {
                $vars[$checkbox_field] = 'false';
            }
        }

        $this->Input->setRules($this->getRowRules($vars));

        // Validate module row
        if ($this->Input->validates($vars)) {
            // Build the meta data for this row
            $meta = [];
            foreach ($vars as $key => $value) {
                if (in_array($key, $meta_fields)) {
                    $meta[] = [
                        'key' => $key,
                        'value' => $value,
                        'encrypted' => in_array($key, $encrypted_fields) ? 1 : 0
                    ];
                }
            }

            return $meta;
        }
    }

    /**
     * Deletes the module row on the remote server. Sets Input errors on failure,
     * preventing the row from being deleted.
     *
     * @param stdClass $module_row The stdClass representation of the existing module row
     */
    public function deleteModuleRow($module_row)
    {
    }

    /**
     * Builds and returns the rules required to add/edit a module row (e.g. server).
     *
     * @param array $vars An array of key/value data pairs
     * @return array An array of Input rules suitable for Input::setRules()
     */
    private function getRowRules(&$vars)
    {
////// Defined below are a few common and useful validation functions.  To use them you can change the 'rule' line to
////// something along the lines of:
//////                    'rule' => [[$this, 'validateHostName']],
////// For more information on writing validation rules, see the
////// docs at https://docs.blesta.com/display/dev/Error+Checking
////
        $rules = [
        ];

        return $rules;
    }
////
////    /**
////     * Validates that the given hostname is valid.
////     *
////     * @param string $host_name The host name to validate
////     * @return bool True if the hostname is valid, false otherwise
////     */
////    public function validateHostName($host_name)
////    {
////// Be sure to uncomment the Server use statement at the top of this file if you are going to uncomment this method
////        $validator = new Server();
////        return $validator->isDomain($host_name) || $validator->isIp($host_name);
////    }
////
////    /**
////     * Validates that at least 2 name servers are set in the given array of name servers.
////     *
////     * @param array $name_servers An array of name servers
////     * @return bool True if the array count is >= 2, false otherwise
////     */
////    public function validateNameServerCount($name_servers)
////    {
////        if (is_array($name_servers) && count($name_servers) >= 2) {
////            return true;
////        }
////
////        return false;
////    }
////
////    /**
////     * Validates that the nameservers given are formatted correctly.
////     *
////     * @param array $name_servers An array of name servers
////     * @return bool True if every name server is formatted correctly, false otherwise
////     */
////    public function validateNameServers($name_servers)
////    {
////// Be sure that you have also uncommented validateHostName() before you uncomment this method
////        if (is_array($name_servers)) {
////            foreach ($name_servers as $name_server) {
////                if (!$this->validateHostName($name_server)) {
////                    return false;
////                }
////            }
////        }
////
////        return true;
////    }
////
////
////    /**
////     * Validates whether or not the connection details are valid by attempting to fetch
////     * the number of accounts that currently reside on the server.
////     *
////     * @param string $password The ISPmanager server password
////     * @param string $hostname The ISPmanager server hostname
////     * @param string $user_name The ISPmanager server user name
////     * @param mixed $use_ssl Whether or not to use SSL
////     * @param int $account_count The number of existing accounts on the server
////     * @return bool True if the connection is valid, false otherwise
////     */
////    public function validateConnection($password, $hostname, $user_name, $use_ssl, &$account_count)
////    {
////        try {
////// Be sure that you've uncommented the getApi() method if you're uncommenting this code
//////            $api = $this->getApi($hostname, $user_name, $password, $use_ssl);
////
////            $params = compact('hostname', 'user_name', 'password', 'use_ssl');
////            $masked_params = $params;
////            $masked_params['user_name'] = '***';
////            $masked_params['password'] = '***';
////
////            $this->log($hostname . '|user', serialize($masked_params), 'input', true);
////
////            $response = $api->getAccounts();
////
////            $success = false;
////            if (!isset($response->error)) {
////                $account_count = isset($response->response) ? count($response->response) : 0;
////                $success = true;
////            }
////
////            $this->log($hostname . '|user', serialize($response), 'output', $success);
////
////            if ($success) {
////                return true;
////            }
////        } catch (Exception $e) {
////            // Trap any errors encountered, could not validate connection
////        }
////
////        return false;
////    }


    /**
     * Returns an array of available service deligation order methods. The module
     * will determine how each method is defined. For example, the method "first"
     * may be implemented such that it returns the module row with the least number
     * of services assigned to it.
     *
     * @return array An array of order methods in key/value paris where the key is the
     *  type to be stored for the group and value is the name for that option
     * @see Module::selectModuleRow()
     */
    public function getGroupOrderOptions()
    {
        return [
            'roundrobin' => Language::_('Myplugin.order_options.roundrobin', true),
            'first' => Language::_('Myplugin.order_options.first', true)
        ];
    }

    /**
     * Determines which module row should be attempted when a service is provisioned
     * for the given group based upon the order method set for that group.
     *
     * @return int The module row ID to attempt to add the service with
     * @see Module::getGroupOrderOptions()
     */
    public function selectModuleRow($module_group_id)
    {
        if (!isset($this->ModuleManager)) {
            Loader::loadModels($this, ['ModuleManager']);
        }

        $group = $this->ModuleManager->getGroup($module_group_id);

        if ($group) {
            switch ($group->add_order) {
                default:
                case 'first':
////
////                    foreach ($group->rows as $row) {
////                        return $row->id;
////                    }
////
                    break;
                case 'roundrobin':
                    break;
            }
        }
        return 0;
    }

    /**
     * Validates input data when attempting to add a package, returns the meta
     * data to save when adding a package. Performs any action required to add
     * the package on the remote server. Sets Input errors on failure,
     * preventing the package from being added.
     *
     * @param array An array of key/value pairs used to add the package
     * @return array A numerically indexed array of meta fields to be stored for this package containing:
     *
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function addPackage(array $vars = null)
    {
        // Set rules to validate input data
        $this->Input->setRules($this->getPackageRules($vars));

        // Build meta data to return
        $meta = [];
        if ($this->Input->validates($vars)) {
            if (!isset($vars['meta'] )) {
                return [];
            }

            // Return all package meta fields
            foreach ($vars['meta'] as $key => $value) {
                $meta[] = [
                    'key' => $key,
                    'value' => $value,
                    'encrypted' => 0
                ];
            }
        }

        return $meta;
    }

    /**
     * Validates input data when attempting to edit a package, returns the meta
     * data to save when editing a package. Performs any action required to edit
     * the package on the remote server. Sets Input errors on failure,
     * preventing the package from being edited.
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @param array An array of key/value pairs used to edit the package
     * @return array A numerically indexed array of meta fields to be stored for this package containing:
     *
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function editPackage($package, array $vars = null)
    {
        // Set rules to validate input data
        $this->Input->setRules($this->getPackageRules($vars));

        // Build meta data to return
        $meta = [];
        if ($this->Input->validates($vars)) {
            if (!isset($vars['meta'] )) {
                return [];
            }

            // Return all package meta fields
            foreach ($vars['meta'] as $key => $value) {
                $meta[] = [
                    'key' => $key,
                    'value' => $value,
                    'encrypted' => 0
                ];
            }
        }

        return $meta;
    }

    /**
     * Deletes the package on the remote server. Sets Input errors on failure,
     * preventing the package from being deleted.
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function deletePackage($package)
    {
    }

    /**
     * Builds and returns rules required to be validated when adding/editing a package.
     *
     * @param array $vars An array of key/value data pairs
     * @return array An array of Input rules suitable for Input::setRules()
     */
    private function getPackageRules(array $vars)
    {
////// For info on writing validation rules, see the
////// docs at https://docs.blesta.com/display/dev/Error+Checking
////
        // Validate the package fields
        $rules = [
            'epp_code' => [
                'valid' => [
                    'rule' => true,
                    'message' => Language::_('Myplugin.!error.epp_code.valid', true)
                ]
            ]
        ];

        return $rules;
    }

    /**
     * Returns all fields used when adding/editing a package, including any
     * javascript to execute when the page is rendered with these fields.
     *
     * @param $vars stdClass A stdClass object representing a set of post fields
     * @return ModuleFields A ModuleFields object, containg the fields to
     *  render as well as any additional HTML markup to include
     */
    public function getPackageFields($vars = null)
    {
        Loader::loadHelpers($this, ['Html']);

        $fields = new ModuleFields();

        // Set the EPP Code field
        $epp_code = $fields->label(Language::_('Myplugin.package_fields.epp_code', true), 'myplugin_epp_code');
        $epp_code->attach(
            $fields->fieldCheckbox(
                'meta[epp_code]',
                'true',
                (isset($vars->meta['epp_code']) ? $vars->meta['epp_code'] : null) == 'true',
                ['id' => 'myplugin_epp_code']
            )
        );
        // Add tooltip
        $tooltip = $fields->tooltip(Language::_('Myplugin.package_field.tooltip.epp_code', true));
        $epp_code->attach($tooltip);
        $fields->setField($epp_code);

        return $fields;
    }

    /**
     * Adds the service to the remote server. Sets Input errors on failure,
     * preventing the service from being added.
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @param array $vars An array of user supplied info to satisfy the request
     * @param stdClass $parent_package A stdClass object representing the parent
     *  service's selected package (if the current service is an addon service)
     * @param stdClass $parent_service A stdClass object representing the parent
     *  service of the service being added (if the current service is an addon service
     *  service and parent service has already been provisioned)
     * @param string $status The status of the service being added. These include:
     *  - active
     *  - canceled
     *  - pending
     *  - suspended
     * @return array A numerically indexed array of meta fields to be stored for this service containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function addService(
        $package,
        array $vars = null,
        $parent_package = null,
        $parent_service = null,
        $status = 'pending'
    ) {
////// Modules often load an API object to perform necessary actions on the remote server.  Below is some code
////// to ensure that we have a module row to connect to the server and to load the API object.  You will want
////// to replace the parameters being submitted to the method with those relevant for your module.  Uncomment
////// the getApi() method below and modify the parameters and doc comments
////        $row = $this->getModuleRow();
////
////        if (!$row) {
////            $this->Input->setErrors(
////                ['module_row' => ['missing' => Language::_('Myplugin.!error.module_row.missing', true)]]
////            );
////
////            return;
////        }
////
////        $api = $this->getApi($row->meta->host_name, $row->meta->user_name, $row->meta->password, $row->meta->use_ssl);
////
////// Modules often find it useful to do some processing and formatting before submitting data to the API.  Uncomment
////// the getFieldsFromInput() method below and update it to suite your needs.
////        $params = $this->getFieldsFromInput((array) $vars, $package);

        // Set unset checkboxes
        $checkbox_fields = [];

        foreach ($checkbox_fields as $checkbox_field) {
            if (!isset($vars[$checkbox_field])) {
                $vars[$checkbox_field] = 'false';
            }
        }


        $this->validateService($package, $vars);

        if ($this->Input->errors()) {
            return;
        }

        // Only provision the service if 'use_module' is true
        if ($vars['use_module'] == 'true') {
        }

        // Return service fields
        return [
            [
                'key' => 'domain',
                'value' => $vars['domain'],
                'encrypted' => 0
            ]
        ];
    }

    /**
     * Edits the service on the remote server. Sets Input errors on failure,
     * preventing the service from being edited.
     *
     * @param stdClass $package A stdClass object representing the current package
     * @param stdClass $service A stdClass object representing the current service
     * @param array $vars An array of user supplied info to satisfy the request
     * @param stdClass $parent_package A stdClass object representing the parent
     *  service's selected package (if the current service is an addon service)
     * @param stdClass $parent_service A stdClass object representing the parent
     *  service of the service being edited (if the current service is an addon service)
     * @return array A numerically indexed array of meta fields to be stored for this service containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function editService($package, $service, array $vars = null, $parent_package = null, $parent_service = null)
    {
////// Modules often load an API object to perform necessary actions on the remote server.  Below is some code
////// to ensure that we have a module row to connect to the server and to load the API object.  You will want
////// to replace the parameters being submitted to the method with those relevant for your module.  Uncomment
////// the getApi() method below and modify the parameters and doc comments
////        $row = $this->getModuleRow();
////
////        if (!$row) {
////            $this->Input->setErrors(
////                ['module_row' => ['missing' => Language::_('Myplugin.!error.module_row.missing', true)]]
////            );
////
////            return;
////        }
////
////        $api = $this->getApi($row->meta->host_name, $row->meta->user_name, $row->meta->password, $row->meta->use_ssl);
////
////// Modules often find it useful to do some processing and formatting before submitting data to the API.  Uncomment
////// the getFieldsFromInput() method below and update it to suite your needs.
////        $params = $this->getFieldsFromInput((array) $vars, $package);

        // Set unset checkboxes
        $checkbox_fields = [];

        foreach ($checkbox_fields as $checkbox_field) {
            if (!isset($vars[$checkbox_field])) {
                $vars[$checkbox_field] = 'false';
            }
        }

        $service_fields = $this->serviceFieldsToObject($service->fields);

        $this->validateService($package, $vars, true);

        if ($this->Input->errors()) {
            return;
        }

        // Only update the service if 'use_module' is true
        if ($vars['use_module'] == 'true') {
        }

        // Return all the service fields
        $encrypted_fields = [];
        $return = [];
        $fields = ['domain'];
        foreach ($fields as $field) {
            if (isset($vars[$field]) || isset($service_fields[$field])) {
                $return[] = [
                    'key' => $field,
                    'value' => $vars[$field] ?? $service_fields[$field],
                    'encrypted' => (in_array($field, $encrypted_fields) ? 1 : 0)
                ];
            }
        }

        return $return;
    }

    /**
     * Suspends the service on the remote server. Sets Input errors on failure,
     * preventing the service from being suspended.
     *
     * @param stdClass $package A stdClass object representing the current package
     * @param stdClass $service A stdClass object representing the current service
     * @param stdClass $parent_package A stdClass object representing the parent
     *  service's selected package (if the current service is an addon service)
     * @param stdClass $parent_service A stdClass object representing the parent
     *  service of the service being suspended (if the current service is an addon service)
     * @return mixed null to maintain the existing meta fields or a numerically
     *  indexed array of meta fields to be stored for this service containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function suspendService($package, $service, $parent_package = null, $parent_service = null)
    {
        if (($row = $this->getModuleRow())) {
////            $api = $this->getApi(
////                $row->meta->host_name,
////                $row->meta->user_name,
////                $row->meta->password,
////                $row->meta->use_ssl
////            );
////
////            $service_fields = $this->serviceFieldsToObject($service->fields);
        }

        return null;
    }

    /**
     * Unsuspends the service on the remote server. Sets Input errors on failure,
     * preventing the service from being unsuspended.
     *
     * @param stdClass $package A stdClass object representing the current package
     * @param stdClass $service A stdClass object representing the current service
     * @param stdClass $parent_package A stdClass object representing the parent
     *  service's selected package (if the current service is an addon service)
     * @param stdClass $parent_service A stdClass object representing the parent
     *  service of the service being unsuspended (if the current service is an addon service)
     * @return mixed null to maintain the existing meta fields or a numerically
     *  indexed array of meta fields to be stored for this service containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function unsuspendService($package, $service, $parent_package = null, $parent_service = null)
    {
        if (($row = $this->getModuleRow())) {
////            $api = $this->getApi(
////                $row->meta->host_name,
////                $row->meta->user_name,
////                $row->meta->password,
////                $row->meta->use_ssl
////            );
////
////            $service_fields = $this->serviceFieldsToObject($service->fields);
        }

        return null;
    }

    /**
     * Cancels the service on the remote server. Sets Input errors on failure,
     * preventing the service from being canceled.
     *
     * @param stdClass $package A stdClass object representing the current package
     * @param stdClass $service A stdClass object representing the current service
     * @param stdClass $parent_package A stdClass object representing the parent
     *  service's selected package (if the current service is an addon service)
     * @param stdClass $parent_service A stdClass object representing the parent
     *  service of the service being canceled (if the current service is an addon service)
     * @return mixed null to maintain the existing meta fields or a numerically
     *  indexed array of meta fields to be stored for this service containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function cancelService($package, $service, $parent_package = null, $parent_service = null)
    {
////        if (($row = $this->getModuleRow())) {
////            $api = $this->getApi(
////                $row->meta->host_name,
////                $row->meta->user_name,
////                $row->meta->password,
////                $row->meta->use_ssl
////            );
////
////            $service_fields = $this->serviceFieldsToObject($service->fields);
////        }
////
        return null;
    }

    /**
     * Allows the module to perform an action when the service is ready to renew.
     * Sets Input errors on failure, preventing the service from renewing.
     *
     * @param stdClass $package A stdClass object representing the current package
     * @param stdClass $service A stdClass object representing the current service
     * @param stdClass $parent_package A stdClass object representing the parent
     *  service's selected package (if the current service is an addon service)
     * @param stdClass $parent_service A stdClass object representing the parent
     *  service of the service being renewed (if the current service is an addon service)
     * @return mixed null to maintain the existing meta fields or a numerically
     *  indexed array of meta fields to be stored for this service containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function renewService($package, $service, $parent_package = null, $parent_service = null)
    {
////        if (($row = $this->getModuleRow())) {
////            $api = $this->getApi(
////                $row->meta->host_name,
////                $row->meta->user_name,
////                $row->meta->password,
////                $row->meta->use_ssl
////            );
////
////            $service_fields = $this->serviceFieldsToObject($service->fields);
////        }
////
        return null;
    }

    /**
     * Attempts to validate service info. This is the top-level error checking method. Sets Input errors on failure.
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @param array $vars An array of user supplied info to satisfy the request
     * @return bool True if the service validates, false otherwise. Sets Input errors when false.
     */
    public function validateService($package, array $vars = null)
    {
        $this->Input->setRules($this->getServiceRules($vars));
        return $this->Input->validates($vars);
    }

    /**
     * Attempts to validate an existing service against a set of service info updates. Sets Input errors on failure.
     *
     * @param stdClass $service A stdClass object representing the service to validate for editing
     * @param array $vars An array of user-supplied info to satisfy the request
     * @return bool True if the service update validates or false otherwise. Sets Input errors when false.
     */
    public function validateServiceEdit($service, array $vars = null)
    {
        $this->Input->setRules($this->getServiceRules($vars, true));
        return $this->Input->validates($vars);
    }

    /**
     * Returns the rule set for adding/editing a service
     *
     * @param array $vars A list of input vars
     * @param bool $edit True to get the edit rules, false for the add rules
     * @return array Service rules
     */
    private function getServiceRules(array $vars = null, $edit = false)
    {
////// For info on writing validation rules, see the
////// docs at https://docs.blesta.com/display/dev/Error+Checking
////
        // Validate the service fields
        $rules = [
            'domain' => [
                'valid' => [
                    'if_set' => $edit,
                    'rule' => true,
                    'message' => Language::_('Myplugin.!error.domain.valid', true)
                ]
            ]
        ];

        // Unset irrelevant rules when editing a service
        if ($edit) {
            $edit_fields = [];

            foreach ($rules as $field => $rule) {
                if (!in_array($field, $edit_fields)) {
                    unset($rules[$field]);
                }
            }
        }

        return $rules;
    }

    /**
     * Updates the package for the service on the remote server. Sets Input
     * errors on failure, preventing the service's package from being changed.
     *
     * @param stdClass $package_from A stdClass object representing the current package
     * @param stdClass $package_to A stdClass object representing the new package
     * @param stdClass $service A stdClass object representing the current service
     * @param stdClass $parent_package A stdClass object representing the parent
     *  service's selected package (if the current service is an addon service)
     * @param stdClass $parent_service A stdClass object representing the parent
     *  service of the service being changed (if the current service is an addon service)
     * @return mixed null to maintain the existing meta fields or a numerically
     *  indexed array of meta fields to be stored for this service containing:
     *  - key The key for this meta field
     *  - value The value for this key
     *  - encrypted Whether or not this field should be encrypted (default 0, not encrypted)
     * @see Module::getModule()
     * @see Module::getModuleRow()
     */
    public function changeServicePackage(
        $package_from,
        $package_to,
        $service,
        $parent_package = null,
        $parent_service = null
    ) {
        if (($row = $this->getModuleRow())) {
////            $api = $this->getApi(
////                $row->meta->host_name,
////                $row->meta->user_name,
////                $row->meta->password,
////                ($row->meta->use_ssl == 'true')
////            );
////
        }

        return null;
    }
////
////    /**
////     * Initializes the MypluginApi and returns an instance of that object.
////     *
////     * @return MypluginApi The MypluginApi instance
////     */
////    private function getApi()
////    {
////        Loader::load(dirname(__FILE__) . DS . 'apis' . DS . 'myplugin_api.php');
////
//////        See the apis/myplugin_api.php and apis/myplugin_response.php files
////        $api = new MypluginApi();
////
////        return $api;
////    }
////
////    /**
////     * Returns an array of service field to set for the service using the given input
////     *
////     * @param array $vars An array of key/value input pairs
////     * @param stdClass $package A stdClass object representing the package for the service
////     * @return array An array of key/value pairs representing service fields
////     */
////    private function getFieldsFromInput(array $vars, $package)
////    {
////        $domain = isset($vars['domain']) ? strtolower($vars['domain']) : null;
////        $username = !empty($vars['username'])
////            ? $vars['username']
////            : $this->generateUsername($domain);
////        $password = !empty($vars['password']) ? $vars['password'] : $this->generatePassword();
////        $fields = [
////            'domain' => $domain,
////            'username' => $username,
////            'password' => $password,
////            'confirm_password' => $password,
////        ];
////
////        return $fields;
////    }
////
////    /**
////     * Generates a username from the given host name.
////     *
////     * @param string $host_name The host name to use to generate the username
////     * @return string The username generated from the given hostname
////     */
////    private function generateUsername($host_name)
////    {
////        // Remove everything except letters and numbers from the domain
////        // ensure no number appears in the beginning
////        $username = ltrim(preg_replace('/[^a-z0-9]/i', '', $host_name), '0123456789');
////
////        $length = strlen($username);
////        $pool = 'abcdefghijklmnopqrstuvwxyz0123456789';
////        $pool_size = strlen($pool);
////
////        if ($length < 5) {
////            for ($i = $length; $i < 8; $i++) {
////                $username .= substr($pool, mt_rand(0, $pool_size - 1), 1);
////            }
////            $length = strlen($username);
////        }
////
////        $username = substr($username, 0, min($length, 8));
////
////        // Check for an existing user account
////        $row = $this->getModuleRow();
////
////        if ($row) {
////            $api = $this->getApi($row->meta->host_name, $row->meta->user_name, $row->meta->password, $row->meta->use_ssl);
////
////            // Username exists, create another instead
////            if ($api->accountExists($username)) {
////                for ($i = 0; strlen((string)$i) < 8; $i++) {
////                    $new_username = substr($username, 0, -strlen((string)$i)) . $i;
////                    if (!$api->accountExists($new_username)) {
////                        $username = $new_username;
////                        break;
////                    }
////                }
////            }
////        }
////
////        return $username;
////    }
////
////    /**
////     * Generates a password.
////     *
////     * @param int $min_length The minimum character length for the password (5 or larger)
////     * @param int $max_length The maximum character length for the password (14 or fewer)
////     * @return string The generated password
////     */
////    private function generatePassword($min_length = 10, $max_length = 14)
////    {
////        $pool = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()';
////        $pool_size = strlen($pool);
////        $length = mt_rand(max($min_length, 5), min($max_length, 14));
////        $password = '';
////
////        for ($i = 0; $i < $length; $i++) {
////            $password .= substr($pool, mt_rand(0, $pool_size - 1), 1);
////        }
////
////        return $password;
////    }


    /**
     * Fetches the HTML content to display when viewing the service info in the
     * admin interface.
     *
     * @param stdClass $service A stdClass object representing the service
     * @param stdClass $package A stdClass object representing the service's package
     * @return string HTML content containing information to display when viewing the service info
     */
    public function getAdminServiceInfo($service, $package)
    {
        $row = $this->getModuleRow();

        // Load the view into this object, so helpers can be automatically added to the view
        $this->view = new View('admin_service_info', 'default');
        $this->view->base_uri = $this->base_uri;
        $this->view->setDefaultView('components' . DS . 'modules' . DS . 'myplugin' . DS);

        // Load the helpers required for this view
        Loader::loadHelpers($this, ['Form', 'Html']);

        $this->view->set('module_row', $row);
        $this->view->set('package', $package);
        $this->view->set('service', $service);
        $this->view->set('service_fields', $this->serviceFieldsToObject($service->fields));

        return $this->view->fetch();
    }

    /**
     * Fetches the HTML content to display when viewing the service info in the
     * client interface.
     *
     * @param stdClass $service A stdClass object representing the service
     * @param stdClass $package A stdClass object representing the service's package
     * @return string HTML content containing information to display when viewing the service info
     */
    public function getClientServiceInfo($service, $package)
    {
        $row = $this->getModuleRow();

        // Load the view into this object, so helpers can be automatically added to the view
        $this->view = new View('client_service_info', 'default');
        $this->view->base_uri = $this->base_uri;
        $this->view->setDefaultView('components' . DS . 'modules' . DS . 'myplugin' . DS);

        // Load the helpers required for this view
        Loader::loadHelpers($this, ['Form', 'Html']);

        $this->view->set('module_row', $row);
        $this->view->set('package', $package);
        $this->view->set('service', $service);
        $this->view->set('service_fields', $this->serviceFieldsToObject($service->fields));

        return $this->view->fetch();
    }

    /**
     * Returns all fields to display to an admin attempting to add a service with the module
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @param $vars stdClass A stdClass object representing a set of post fields
     * @return ModuleFields A ModuleFields object, containg the fields to render
     *  as well as any additional HTML markup to include
     */
    public function getAdminAddFields($package, $vars = null)
    {
        Loader::loadHelpers($this, ['Html']);

        $fields = new ModuleFields();

        // Set the Domain field
        $domain = $fields->label(Language::_('Myplugin.service_fields.domain', true), 'myplugin_domain');
        $domain->attach(
            $fields->fieldText(
                'domain',
                (isset($vars->domain) ? $vars->domain : null),
                ['id' => 'myplugin_domain']
            )
        );
        $fields->setField($domain);

        return $fields;
    }

    /**
     * Returns all fields to display to an admin attempting to edit a service with the module
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @param $vars stdClass A stdClass object representing a set of post fields
     * @return ModuleFields A ModuleFields object, containg the fields to render
     *  as well as any additional HTML markup to include
     */
    public function getAdminEditFields($package, $vars = null)
    {
        Loader::loadHelpers($this, ['Html']);

        $fields = new ModuleFields();

        // Set the Domain field
        $domain = $fields->label(Language::_('Myplugin.service_fields.domain', true), 'myplugin_domain');
        $domain->attach(
            $fields->fieldText(
                'domain',
                (isset($vars->domain) ? $vars->domain : null),
                ['id' => 'myplugin_domain']
            )
        );
        $fields->setField($domain);

        return $fields;
    }

    /**
     * Returns all fields to display to a client attempting to add a service with the module
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @param $vars stdClass A stdClass object representing a set of post fields
     * @return ModuleFields A ModuleFields object, containg the fields to render
     *  as well as any additional HTML markup to include
     */
    public function getClientAddFields($package, $vars = null)
    {
        Loader::loadHelpers($this, ['Html']);

        $fields = new ModuleFields();

        // Set the Domain field
        $domain = $fields->label(Language::_('Myplugin.service_fields.domain', true), 'myplugin_domain');
        $domain->attach(
            $fields->fieldText(
                'domain',
                (isset($vars->domain) ? $vars->domain : null),
                ['id' => 'myplugin_domain']
            )
        );
        $fields->setField($domain);

        return $fields;
    }

    /**
     * Returns all fields to display to a client attempting to edit a service with the module
     *
     * @param stdClass $package A stdClass object representing the selected package
     * @param $vars stdClass A stdClass object representing a set of post fields
     * @return ModuleFields A ModuleFields object, containg the fields to render
     *  as well as any additional HTML markup to include
     */
    public function getClientEditFields($package, $vars = null)
    {
        Loader::loadHelpers($this, ['Html']);

        $fields = new ModuleFields();

        // Set the Domain field
        $domain = $fields->label(Language::_('Myplugin.service_fields.domain', true), 'myplugin_domain');
        $domain->attach(
            $fields->fieldText(
                'domain',
                (isset($vars->domain) ? $vars->domain : null),
                ['id' => 'myplugin_domain']
            )
        );
        $fields->setField($domain);

        return $fields;
    }
}
