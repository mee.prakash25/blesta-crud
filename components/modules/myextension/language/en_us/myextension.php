<?php
/**
 * en_us language for the MyExtension module.
 */
// Basics
$lang['Myextension.name'] = 'MyExtension';
$lang['Myextension.description'] = 'My Test extension';
$lang['Myextension.module_row'] = 'MyServer';
$lang['Myextension.module_row_plural'] = 'MyServers';
$lang['Myextension.module_group'] = 'My Server Group';


// Module management
$lang['Myextension.add_module_row'] = 'Add MyServer';
$lang['Myextension.add_module_group'] = 'Add My Server Group';
$lang['Myextension.manage.module_rows_title'] = 'MyServers';

$lang['Myextension.manage.module_rows_heading.options'] = 'Options';
$lang['Myextension.manage.module_rows.edit'] = 'Edit';
$lang['Myextension.manage.module_rows.delete'] = 'Delete';
$lang['Myextension.manage.module_rows.confirm_delete'] = 'Are you sure you want to delete this MyServer';

$lang['Myextension.manage.module_rows_no_results'] = 'There are no MyServers.';

$lang['Myextension.manage.module_groups_title'] = 'Groups';
$lang['Myextension.manage.module_groups_heading.name'] = 'Name';
$lang['Myextension.manage.module_groups_heading.module_rows'] = 'MyServers';
$lang['Myextension.manage.module_groups_heading.options'] = 'Options';

$lang['Myextension.manage.module_groups.edit'] = 'Edit';
$lang['Myextension.manage.module_groups.delete'] = 'Delete';
$lang['Myextension.manage.module_groups.confirm_delete'] = 'Are you sure you want to delete this MyServer';

$lang['Myextension.manage.module_groups.no_results'] = 'There is no My Server Group';


$lang['Myextension.order_options.roundrobin'] = 'Evenly Distribute Among Servers';
$lang['Myextension.order_options.first'] = 'First Non-full Server';


// Add row
$lang['Myextension.add_row.box_title'] = 'MyExtension - Add MyServer';
////$lang['Myextension.add_row.name_servers_title'] = 'Name Servers';
////$lang['Myextension.add_row.notes_title'] = 'Notes';
////$lang['Myextension.add_row.name_server_btn'] = 'Add Additional Name Server';
////$lang['Myextension.add_row.name_server_col'] = 'Name Server';
////$lang['Myextension.add_row.name_server_host_col'] = 'Hostname';
////$lang['Myextension.add_row.name_server'] = 'Name server %1$s'; // %1$s is the name server number (e.g. 3)
////$lang['Myextension.add_row.remove_name_server'] = 'Remove';
$lang['Myextension.add_row.add_btn'] = 'Add MyServer';


// Edit row
$lang['Myextension.edit_row.box_title'] = 'MyExtension - Edit MyServer';
////$lang['Myextension.edit_row.name_servers_title'] = 'Name Servers';
////$lang['Myextension.edit_row.notes_title'] = 'Notes';
////$lang['Myextension.edit_row.name_server_btn'] = 'Add Additional Name Server';
////$lang['Myextension.edit_row.name_server_col'] = 'Name Server';
////$lang['Myextension.edit_row.name_server_host_col'] = 'Hostname';
////$lang['Myextension.edit_row.name_server'] = 'Name server %1$s'; // %1$s is the name server number (e.g. 3)
////$lang['Myextension.edit_row.remove_name_server'] = 'Remove';
$lang['Myextension.edit_row.edit_btn'] = 'Update MyServer';


// Row meta




// Errors
$lang['Myextension.!error.module_row.missing'] = 'An internal error occurred. The module row is unavailable.';


// Service info
$lang['Myextension.service_info.domain'] = 'Domain';
////// These are the definitions for if you are trying to include a login link in the service info pages
////$lang['Myextension.service_info.options'] = 'Options';
////$lang['Myextension.service_info.option_login'] = 'Login';

// Service Fields
$lang['Myextension.service_fields.domain'] = 'Domain';




// Package Fields
$lang['Myextension.package_fields.epp_code'] = 'EPP Code';


$lang['Myextension.package_field.tooltip.epp_code'] = 'Whether to allow users to request an EPP Code through the Blesta service interface.';

// Cron Tasks

